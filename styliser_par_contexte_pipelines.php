<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function styliser_par_contexte_styliser($flux) {
	$args = $flux['args'];

	if (isset($args['contexte']['styliser_par']) and $args['contexte']['styliser_par']) {
		$flux['data'] .= '_'.$args['contexte']['styliser_par'];
	}

	return $flux;
}
