# styliser_par_contexte

Ce plugin permet des variantes de squelettes selon une valeur d'environnement du squelette
<INCLURE{fond=monjolifond,styliser_par=#GET{untruc}>

Ça ressemble aux compositions, mais il n'y a pas de champ 'composition' créé dans toutes les tables.

Il est peu utile puisque quand on inclue le fond, on peut calculer son inclure avec SPIP à partir du contexte.